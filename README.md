# BashBook
BashBook is a simple, automated diary script written in Bash. It automatically creates, prepopulates and opens a new diary document upon first reboot after opening the terminal.

## Roadmap and Goals
The goal for this program is simplicity. It's purpose is to easily be able to make journal entries first thing in the morning. Other, minor goals are:

- Security - Optional encryption for diary documents (not yet implemented)
- Ease of use
